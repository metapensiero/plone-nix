{pkgs ? import <nixpkgs> {}, ploneVersion ? "5.2.5", pythonVersion ? "3",
  extractRequirements ? false}:
  let
    python = pkgs."python${pythonVersion}";
    pyNix = import ./. {inherit pkgs python;};
    ploneNix = pyNix.plone {inherit ploneVersion;};
  in ploneNix.mkShell {
    inherit extractRequirements;
    varDir = "${builtins.getEnv "PWD"}/var";
  }
