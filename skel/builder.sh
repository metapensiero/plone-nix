#!/usr/bin/env bash
# -*- coding: utf-8 -*-
# :Project:   plone-nix -- skeleton generator
# :Created:   mer 4 nov 2020, 01:46:45
# :Author:    Alberto Berti <alberto@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2020 Alberto Berti
#
set -euo pipefail

source $stdenv/setup

mkdir -p $out/bin;
echo "Generating plone skeleton..."
cd $out && buildout-gen-plone-skel

echo "Fixing files..."
rm -rf .installed eggs develop-eggs
# remove insertions of the site packages
sed -i 10,12d bin/instance
sed -i 13,15d parts/instance/bin/interpreter
find -type f | xargs sed -i "s#$out/var#$varDir#"

echo "Generating copy script..."
sed "s#OUTDIR#$out#" $src/copy_skel.sh | sed "s#DESTDIR#$varDir#" \
    > $out/bin/copy-plone-skeleton
chmod +x $out/bin/copy-plone-skeleton
