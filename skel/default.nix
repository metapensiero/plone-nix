# -*- coding: utf-8 -*-
# :Project:   plone-nix -- buildout assist
# :Created:   mer 4 nov 2020, 01:39:33
# :Author:    Alberto Berti <alberto@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2020 Alberto Berti
#

{ buildoutPy, coreutils, findutils,
  instanceCfg ? {http-address = "8080"; user = "admin:admin";},
  lib, plonePy, ploneVersion, gnused, stdenvNoCC, writeShellScriptBin,
  writeText}:
  let
    inherit (lib) concatStringsSep mapAttrsToList;
    /* Build a `buildout` (eh) configuration suitable to install the given
       Plone version */
    buildoutInstallCfg = writeText "buildout-install-${ploneVersion}.cfg" ''
      [buildout]
      extensions = buildout.requirements
      overwrite-requirements-file = true
      extends =
          http://dist.plone.org/release/${ploneVersion}/versions.cfg

      parts =
          instance

      [instance]
      recipe = plone.recipe.zope2instance
      user = admin:admin
      http-address = 8080
      eggs =
          Plone

      [versions]
      # needed or it will abort installation
      setuptools = 47.1.0
    '';
    buildoutSkelCfg = config:
      let
        finalConfig = config // {executable = plonePy.python.interpreter;};
      in writeText "buildout-skel-${ploneVersion}.cfg" ''
        [buildout]
        offline = true
        newest = false
        parts =
            instance

        [instance]
        recipe = plone.recipe.zope2instance
        eggs =
        include-site-packages = false
        relative-paths = true
        ${concatStringsSep "\n" (
          mapAttrsToList (k: v: "${k} = ${v}") finalConfig
        )}
        [versions]
        # needed or it will abort installation
        # setuptools = 47.1.0
    '';
    buildoutOmeletteCfg = writeText "buildout-omelette-${ploneVersion}.cfg" ''
      [buildout]
      offline = true
      newest = false
      parts =
          omelette
      [omelette]
      recipe = collective.recipe.omelette
      eggs =
          Plone
      [versions]
      # needed or it will abort installation
      #setuptools = 47.1.0
    '';
  in {
    install = writeShellScriptBin "buildout-install-plone" ''
      DEST=''${1:-$PWD}
      ${buildoutPy}/bin/buildout -c ${buildoutInstallCfg} \
        buildout:directory="$DEST" install
    '';
    omelette = writeShellScriptBin "buildout-gen-plone-omelette" ''
      DEST=''${1:-$PWD}
      ${plonePy}/bin/buildout -c ${buildoutOmeletteCfg} \
        buildout:directory="$DEST" install
    '';
    skel = instanceLastCfg: varDir:
      let
        mkPloneSkeleton = writeShellScriptBin "buildout-gen-plone-skel" ''
          DEST=''${1:-$PWD}
          ${plonePy}/bin/buildout -c ${buildoutSkelCfg (instanceCfg // instanceLastCfg)} \
          buildout:directory=$DEST $@
        '';
      in if varDir == null
        then mkPloneSkeleton
        else stdenvNoCC.mkDerivation {
          name = "plone-${ploneVersion}-skeleton";
          buildInputs = [ coreutils findutils gnused mkPloneSkeleton ];
          builder = ./builder.sh;
          src = ./.;
          inherit varDir;
        };
  }
