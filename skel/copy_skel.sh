#!/usr/bin/env bash
## -*- coding: utf-8 -*-
# :Project:   plone-nix --
# :Created:   mer 4 nov 2020, 01:46:05
# :Author:    Alberto Berti <alberto@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2020 Alberto Berti
#

set -euo pipefail

mkdir -p DESTDIR
cp -r OUTDIR/var/* DESTDIR
chmod u+rwX -R DESTDIR
