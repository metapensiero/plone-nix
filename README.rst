.. -*- coding: utf-8 -*-
.. :Project:   plone-nix -- readme
.. :Created:   ven 6 nov 2020, 15:00:39
.. :Author:    Alberto Berti <alberto@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2020 Alberto Berti
..

===========================================
 Plone package and dev environment for Nix
===========================================

Welcome, this is a repository containing tools for integrating Plone with Nix__. More specifically, this provides:

* a nix *environment* of the wanted Python 3 interpreter integrated with Plone
  and all its dependencies together with custom packages;

* a package containing the usual Plone scaffold produced by the version of the
  ``plone.recipes.zope2instance`` that the wanted version of Plone ships with;

* a *nix shell* recipe that makes possible to have a development environment with
  Plone and the scaffold;

* and a few othe other goodies.

As of now this supports the following  Plone versions:

* 5.2.2
* 5.2.3-pending

__ https://nixos.org

How to generate and use the Plone environment
=============================================

You can generate the environment with:

.. code:: nix

  {pks, config, ...}:
    let
      myPlonePkg = ../path/to/your/addon/repo;
      ploneNix = import (fetchGit {
        url = "https://gitlab.com/metapensiero/plone-nix.git";
        rev = "xxx";
        sha256 = "xxx";
      });
      plone = ploneNix {
        inherit pkgs;
        packagesExtra = [
          myPlonePkg
        ];
      };

here the ``ploneNix`` call supports some other arguments. The most relevant are:

* ``python`` which is the Python package to be used, defaults to ``python3``;
* ``ploneVersion`` which is the Plone release to build, that is by default the
  latest available.

Then, the generated ``plone`` will be an attrs with the following members:

* ``plonePy`` which is a full Plone environment;
* ``buildoutPy`` which is a Python environronment to just run ``buildout``
* ``buildout`` which is an attrs with:
  - ``install`` an script named ``buildout-install-plone`` that will install the
  configured Plone and then dump the picked versions using ``buildoutPy``;

  - ``skel`` a function that takes an optional flat configuration mapping
    containing options to the ``plone.recipes.zope2instance`` stanza used to
    generate the scaffolding and the Zope configuration. It takes also a string
    path to a directory that will be used as *var*;

    It will return a derivation with all the usual buildout scaffold and a
    script named ``copy-plone-skeleton`` that will copy the tree of scaffolds to
    the designated directory. The ``bin/instance`` script and the usual
    ``zope.conf`` file are built with references to the designated *var*
    directory.

    The default ``zope2instance`` stanza contents are::

      user = admin:admin
      http-address = 8080

Back to the usage, you can then use the ``plone`` attrs as follows to install a
plone service with var in ``/var/lib/mysite`` and running as user ``plone``:

.. code:: nix

  {pkgs, config, ...}:
    let
      myPlonePkg = ../path/to/your/addon/repo;
      siteName = "mysite";
      user = "plone";
      home = "/var/lib/${siteName}";
      adminPw = "changeme"
      port = "4002";
      instanceConfig = {
        user = "admin:${adminPw}";
        http-address = "127.0.0.1:${port}";
        effective-user = user;
      };
      ploneNix = import (fetchGit {
        url = "https://gitlab.com/metapensiero/plone-nix.git";
        rev = "xxx";
        sha256 = "xxx";
      });
      plone = ploneNix {
        inherit pkgs;
        packagesExtra = [
          myPlonePkg
        ];
      };
      ploneSkel = plone.buildout.skel instanceConfig "${home}/var";
    in {
      users.users.${user} = {
        isNormalUser = true;
        name = siteName;
        createHome = false;
      };
      environment.systemPackages = [
        ploneSkel
      ];
      systemd.services.plone = {
        enable = true;
        path = [ ploneSkel pkgs.coreutils pkgs.bash ];
        preStart = ''
          if ! [ -d ${home}/var ]; then
            mkdir -p ${home}
            ${ploneSkel}/bin/copy-plone-skeleton
            chown -R ${user}.users ${home}
          fi
        '';
        serviceConfig = {
          Group = "users";
          Type = "forking";
          ExecReload = "${ploneSkel}/bin/instance restart";
          ExecStart = "${ploneSkel}/bin/instance start";
          ExecStop = "${ploneSkel}/bin/instance stop";
          WorkingDirectory = "~";
        };
        wantedBy = [
          "multi-user.target"
        ];
      };
    }

How to create a Plone development environment
=============================================

The second main feature is the ability to create development environments. That
can be achieved with something like the following:

.. code:: nix

  {pkgs ? import <nixpkgs> {}, ploneVersion ? "5.2.3-pending", pythonVersion ? "3"}:
    let
      packagesExtra = [ ./. ];
      python = pkgs."python${pythonVersion}";
      ploneOptions = {
        inherit packagesExtra pkgs ploneVersion python;
        moreRequirements = ''
          plone.testing[security,z2,zca,zodb]
          plone.testing > =5.0.0
          plone.app.contenttypes
          plone.app.robotframework[debug]
          robotframework == 3.1.2
        '';
        simpleOverrides = {
          # fix various collisions
          "plone.app.robotframework" = {
            postInstall = ''
              rm -f $out/bin/{pybabel,.pybabel-wrapped,robot}
            '';
          };
        };
      };
      ploneNix = import (fetchGit {
        url = "https://gitlab.com/metapensiero/plone-nix.git";
        rev = "e5b34577bb7fc3782362d659b56ff1fea2ef7f16";
      }) ploneOptions;
      inherit (ploneNix) mkPloneShell;
    in mkPloneShell {createVenv = false; varDir = "${builtins.getEnv "PWD"}/var";}
