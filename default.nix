# -*- coding: utf-8 -*-
# :Project:   plone-nix -- main package entry point
# :Created:   mar 3 nov 2020, 23:14:35
# :Author:    Alberto Berti <alberto@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2020 Alberto Berti
#

{ pkgs ? import <nixpkgs> {}, python ? pkgs.python3,
  # ver. 3.3.0
  machNixRev ? "773580c35bcdb8cbd0820018d304686282f88d16",
  machNixSha256 ? "RvbFjxnnY/+/zEkvQpl85MICmyp9p2EoncjuN80yrYA=",
  # see repo https://github.com/davhau/pypi-deps-db
  # updated to 2021-08-13
  pypiDataRev ? "c507bdecb2009b8e67c107510fcea6c69543d184",
  pypiDataSha256 ? "0xjxhfjqsa4rl1acksdcapfqs004frd64wi88xxjgbcxlw1x6210"}:
  let
    inherit (pkgs) fetchFromGitHub;
    inherit (pkgs.lib) concatStringsSep optional optionalString;
    machNixEnv = {
      inherit pkgs;
      python = python.pname;
      inherit pypiDataRev pypiDataSha256;
    };
    machNix = import (fetchFromGitHub {
      owner = "DavHau";
      repo = "mach-nix";
      rev = machNixRev;
      sha256 = machNixSha256;
    }) machNixEnv;
    # machNix = import ./mach-nix machNixEnv;
  in rec {
    inherit machNix pkgs python;
    plone = { moreRequirements ? "", packagesExtra ? [],
              ploneVersion ? "5.2.3-pending", simpleOverrides ? {},
              overridesPost ? []}@attrs:
      let
        additionalRequirements = builtins.readFile
          (./. + "/plone-requirements/additional.txt");
        ploneRequirements = builtins.readFile
          (./. + "/plone-requirements/${ploneVersion}.txt");
        plonePy = machNix.mkPython {
          inherit overridesPost packagesExtra;
          requirements = concatStringsSep "\n" [
            ploneRequirements
            additionalRequirements
            moreRequirements
          ];
          providers = {
            _default = "wheel,nixpkgs,sdist";
            pillow = "nixpkgs";
            lxml = "nixpkgs";
            cssselect = "nixpkgs";
            # pyscss
            # pyrsistent
          };
          _ = simpleOverrides;
        };
        buildout = pkgs.callPackage ./skel
          {inherit buildoutPy plonePy ploneVersion;};
        buildoutPy = machNix.mkPython {
          requirements = ''
            buildout.requirements
            zc.buildout
          '';
          providers = {
            _default = "wheel,nixpkgs,sdist";
          };
        };
      in {
        inherit buildout buildoutPy;
        mkShell = { createVenv ? true,
                    extractRequirements ? false,
                    includeOmelette ? false,
                    instanceCfg ? {},
                    requirementsFile ? "requirements.txt",
                    varDir ? "${builtins.getEnv "PWD"}/var"}:
        let
          ploneSkel = buildout.skel instanceCfg varDir;
          copyVar = optionalString (! extractRequirements) ''
            if ! [ -d ${varDir} ]
            then
              echo "Copying var scaffold to ${varDir}..."
              ${ploneSkel}/bin/copy-plone-skeleton
            fi
          '';
        in pkgs.mkShell rec {
          name = "impurePloneShell-${ploneVersion}";
          venvDir = "./.venv";
          buildInputs = [
            buildout.install
          ] ++ optional (!extractRequirements && includeOmelette) [
            buildout.omelette
          ] ++ (optional (!extractRequirements) [
            plonePy
            ploneSkel
          ]) ++ (optional createVenv [
            # This execute some shell code to initialize a venv in $venvDir
            # before dropping into the shell
            python.pkgs.venvShellHook
          ]);
          preShellHook = ''
            export PYTHONPATH=$(python -S -c "import os; print(os.environ['NIX_PYTHONPATH'])")
          '';
          # Now we can execute any commands within the virtual environment.
          # This is optional and can be left out to run pip manually.
          postShellHook = ''
            ${copyVar}
            # allow pip to install wheels
            unset SOURCE_DATE_EPOCH
            [ -f myenv.sh ] && source myenv.sh
          '';

          postVenvCreation = ''
            unset SOURCE_DATE_EPOCH
            ${optionalString createVenv "pip install -r ${requirementsFile}"}
          '';

        };
        overrideAttrs = f: plone (attrs // (f attrs));
        py = plonePy;
      };
  }
